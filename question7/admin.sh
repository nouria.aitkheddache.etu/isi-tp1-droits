#!/bin/bash

adduser administrateur
usermod -G groupe_a administrateur
usermod -G groupe_b administrateur
su administrateur
mkdir dir_a
chmod g+w dir_a
chmod o-r dir_a
chmod +t dir_a
chmod g+s dir_a
chgrp groupe_a dir_a
touch ./dir_a/a.txt

mkdir dir_b
chmod g+w dir_b
chmod o-r dir_b
chmod +t dir_b
chmod g+s dir_b
chgrp groupe_b dir_b
touch ./dir_b/b.txt

mkdir dir_c
touch ./dir_c/c.txt

