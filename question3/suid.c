#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int ch;
	int ruid  = getuid();
	int rgid = getgid();
	int euid = geteuid();
	int egid = getegid();
	printf("RUID = %d\n", ruid);
	printf("RGID = %d\n", rgid);
	printf("EUID = %d\n", euid);
	printf("EGID = %d\n", egid);

	FILE *f;
	if(argc <2){
		printf("Missing argument \n");
		exit(EXIT_FAILURE);
	}
	f = fopen(argv[1],"r");
	if (f == NULL) {
		perror("Cannot open file");
		exit(EXIT_FAILURE);
	}
	while((ch = fgetc(f)) != EOF)
	{
		putchar(ch);
	}
	fclose(f);
	exit(EXIT_SUCCESS);
}
