#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "check_pass.h"


int main(int argc, char *argv[]){
	//Initialisation
	struct stat sb;
	if (argc < 2){
		printf("Nombre d'arguments incorrect\n");
		exit(EXIT_FAILURE);
	}
	if(stat(argv[1], &sb) == -1){
		perror("stat");
		exit(EXIT_SUCCESS);
	}
	//Check groupe
	int rgid = getgid();
	long fgid = (long) sb.st_gid;
	if(rgid != fgid){
		printf("Les GID ne sont pas les mêmes\n");
		exit(EXIT_FAILURE);
	}
	//Check passwd
	if(check_pass() == 1){
		printf("Le mot de passe n'est pas correct\n");
		exit(EXIT_FAILURE);
	}
	else{
		if(remove(argv[1]) == 0){
			printf("Le fichier a bien été supprimé\n");
		}
		else{
			printf("Le fichier n'a pas été supprimé\n");
		}
	}
	exit(EXIT_SUCCESS);
}
