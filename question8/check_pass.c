#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "check_pass.h"

int check_pass(){
	char pass[20];
	FILE *f;
	printf("Veuillez entrer un mot de passe.\n");
	scanf("%s",pass);
	
	f = fopen("/home/administrateur/passwd","r");
	if (f == NULL){
		perror("Cannot open file");
		exit(EXIT_FAILURE);
	}
	char buf[100];
	while((fgets(buf,100,f) != NULL)) {
		if((strstr(buf,pass)) != NULL){
			printf("Le mot de passe est correct\n");
			return EXIT_SUCCESS;
		}
	}
	fclose(f);
	return EXIT_FAILURE;
}
