# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Ait Kheddache Nouria nouria.aitkheddache.etu@univ-lille.fr

- Nom, Prénom, email: Bouchard Quentin quentin.bouchard.etu@univ-lille.fr

## Question 1

Réponse : Le processus peut accéder en écriture au fichier, parce que le fichier n'est pas accessible en écriture au propriétaire du fichier qui est toto mais il l'est pour tout utilisateur appartenant au même groupe que toto, dont lui-même. 

## Question 2

Réponse : 
- Pour un répertoire, le caractère x signifie pour entrer dans le répertoire et accéder à ces fichiers.
- La permission d'accéder au dossier par toto est refusé parce que toto n'est pas son propriétaire mais fait partie du même groupe que le propriétaire ubuntu, malgré cela le droit "x" pour le groupe a était enlevé donc il ne peut pas y accéder.
- Du fait que toto ne peut accéder à mydir, il ne peut pas accéder à son contenu data.txt donc lors d'une commande ls -al, il renvoie qu'il ne peut pas accéder au répertoire courant mydir '.' et son parent '..' et son contenu 'data.txt', il arrive tous juste à différencier si ce sont des fichiers ou bien des répertoires mais ne connait pas leurs droits.

## Question 3

Réponse :
- RUID = 1001 RGID = 1000 EUID = 1001 EGID = 1000, toto ne peut pas accéder au fichier en lecture.
- RUID = 1001 RGID = 1000 EUID = 1000 EGID = 1000, après activation de set-user-id, toto peut désormais accéder au fichier en lecture.


## Question 4

Réponse : EUID = 1001 EGID = 1000


## Question 5

Réponse : 
- chfn permet de modifier les informations personnel des utilisateurs contenu dans le fichier "/etc/passwd".
- ls -al /usr/bin/chfn affiche : -rwsr-xr-x 1 root root 85064 May 28 2020 /usr/bin/chfn

Le propriétaire root a les droits read et write, le flag set-user-id est levé, les membres du groupe root ont les droits read et execute.
Les autres utilisateurs ont les droits read et execute également.


## Question 6

Réponse :
Les mots de passe sont stockés dans /etc/shadow.
En effet /etc/passwd contient d'autres informations qui eux ont besoin d'être lisible par tous le monde
Mais il ne faut pas que tous les utilisateurs puissent avoir accès aux hashs des mots de passe, un utilisateur malveillant pourrait essayer de les déchiffrer. Voilà pourquoi ces hashs ont étaient mis dans un autre fichier /etc/shadow qui lui ne laisse pas tous le monde y avoir accès.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








