#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <crypt.h>

int check_exist(char *id){
	FILE *f;
	f = fopen("/home/administrateur/passwd","r");
	if (f == NULL){
		perror("Cannot open file");
		exit(EXIT_FAILURE);
	}
	char buf[100];
	while((fgets(buf,100,f) != NULL)){
		if((strstr(buf,id)) != NULL){
			fclose(f);
			return EXIT_SUCCESS;
		}
	}
	fclose(f);
	return EXIT_FAILURE;
}

int check_pass(char *id, const char *salt){
	char *result;
	char original[100];
	FILE *f;
	result = crypt(getpass("Verification du mot de passe:"),salt);
	f = fopen("/home/administrateur/passwd","r");
	if(f == NULL){
		perror("Cannot open file");
		exit(EXIT_FAILURE);
	}
	char buf[100];
	while((fgets(buf,100,f) != NULL)){
		if(strstr(buf,id) != NULL){
			strncpy(original,buf+19,34);
			original[34] = '\0';
			printf("mdp enregistré = %s\n",original);
			printf("mdp saisie = %s\n",result);
			if(strcmp(result,original) == 0){
				fclose(f);
				return EXIT_SUCCESS;
			}
		}
	}
	fclose(f);
	return EXIT_FAILURE;

}

int modify(char *id, const char *salt){
	FILE *f;
	char *result;
	int temp;
	result = crypt(getpass("Nouveau mot de passe:"),salt);
	char str[100];
	sprintf(str, "user=%s password=%s\n",id,result);

	f = fopen("/home/administrateur/passwd","r+");
	if(f == NULL){
		perror("Cannot open file");
		exit(EXIT_FAILURE);
	}
	char buf[100];
	fseek(f, 0, SEEK_SET);
	temp = ftell(f);
	while((fgets(buf,100,f) != NULL)){
		if((strstr(buf,id)) != NULL){
			fseek(f, temp, SEEK_SET);
			//fwrite(str, sizeof(char), strlen(str),f);
			fputs(str,f);
			fclose(f);
			return EXIT_SUCCESS;
		}
		temp = ftell(f);
	}
	fclose(f);
	return EXIT_FAILURE;
}

int create_password(char *id, const char *salt){
	FILE *f;
	char *result;
	result = crypt(getpass("Creation du nouveau mot de passe:"),salt);
	char str[100];
	sprintf(str, "user=%s password=%s\n",id,result);

	f = fopen("/home/administrateur/passwd","a");
	if(f == NULL){
		perror("Cannot open file");
		exit(EXIT_FAILURE);
	}
	fputs(str,f);
	fclose(f);
	return EXIT_SUCCESS;
}

int main(){
	const char *salt = "$1$iSaq7rBk";
	int ruid = getuid();
	char id[10];
	sprintf(id, "%d", ruid);
	//Vérifier si l'utilisateur a un mdp
	if(check_exist(id) == 0){
		printf("Vous avez déjà un mot de passe\n");
		//Vérifier l'ancien mdp
		if(check_pass(id,salt) == 0){
			printf("La vérification du mot de passe est correcte\n");
			//Modifier le mdp
			if(modify(id,salt) == 0){
				printf("La modification du mot de passe s'est bien déroulé\n");
				return EXIT_SUCCESS;
			}
			else{
				printf("La modification du mot de passe n'a pas abouti\n");
				return EXIT_FAILURE;
			}
		}
		else{
			printf("L'ancien mot de passe n'est pas correct\n");
			return EXIT_FAILURE;
		}
	}
	else{
		//En créer un nouveau
		if(create_password(id,salt) == 0){
			printf("Nouveau mot de passe créé\n");
			return EXIT_SUCCESS;
		}
		else{
			printf("La création d'un nouveau mot de passe n'a pas abouti\n");
			return EXIT_FAILURE;
		}
	}
	return EXIT_SUCCESS;
}
